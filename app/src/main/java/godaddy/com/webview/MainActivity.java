package godaddy.com.webview;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//
        setContentView(R.layout.activity_main);
        WebView inappWebView = (WebView) findViewById(R.id.webView);
        inappWebView.setWebChromeClient(new WebChromeClient());
        inappWebView.setWebViewClient(new WebViewClient());
        inappWebView.clearCache(true);
        inappWebView.clearHistory();
        inappWebView.getSettings().setJavaScriptEnabled(true);
        inappWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        inappWebView.loadUrl("http://m.godaddy.com/domains/search.aspx?mDeviceID=e5820eb926e358c8dc48422a54b82462&mappid=3&hidetoolbar=1&ci=13945");
        //1. Http -> https transisition
        //inappWebView.loadUrl("http://m.godaddy.com?task=availcheck&domain=%62%62&tld=&mDeviceID=6212862e2515c390fda18303e74225d9&mappid=3&&hidetoolbar=1");

        // 2. Direct cart load (problem doesn't manifest when cart is empty, had to resort to extreme measures to fill up cart but even problem doesn't
        //manifest
       //inappWebView.loadUrl("https://cart.m.godaddy.com/Cart/Basket.aspx?ci=16555&mDeviceID=6212862e2515c390fda18303e74225d9&mappid=3&&hidetoolbar=1");
//        //https://cart.m.godaddy.com/Cart/Basket.aspx?ci=16555

        //3. Direct html source loading, renders page alright
  //      String html = "<html><head><title>\n" +
//                "Cart\n" +
//                "</title>\n" +
//                "<link rel=\"shortcut icon\" href=\"https://img1.wsimg.com/assets/godaddy.ico\">\n" +
//                "<meta name=\"viewport\" content=\"width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no\">\n" +
//                "<link rel=\"Stylesheet\" type=\"text/css\" href=\"https://img1.wsimg.com/mobile/css/web/css/gdapp_20141028.min.css\">\n" +
//                "<style>\n" +
//                ".cart-savings-link\n" +
//                "{\n" +
//                "text-align: right;\n" +
//                "width: 70%;\n" +
//                "text-decoration: underline;\n" +
//                "}\n" +
//                ".cart-savings-price\n" +
//                "{\n" +
//                "text-align: right;\n" +
//                "margin-left: 10px;\n" +
//                "}\n" +
//                "</style>\n" +
//                "<style>\n" +
//                "#divLoadingModal\n" +
//                "{\n" +
//                "width:225px;\n" +
//                "height:110px;\n" +
//                "padding:5px;\n" +
//                "background:#fff;\n" +
//                "border:1px #777 solid;\n" +
//                "overflow:hidden;\n" +
//                "}\n" +
//                "#divLoadingModal div\n" +
//                "{\n" +
//                "display:table-cell;\n" +
//                "vertical-align:middle;\n" +
//                "}\n" +
//                "#loadingSpinnerText\n" +
//                "{\n" +
//                "height:100px;\n" +
//                "}\n" +
//                "#loadingSpinner\n" +
//                "{\n" +
//                "position:relative;\n" +
//                "width:75px;\n" +
//                "height:100px;\n" +
//                "}\n" +
//                ".modal-cont\n" +
//                "{\n" +
//                "box-shadow:black 0 3px 6px;\n" +
//                "-moz-box-sizing:border-box;\n" +
//                "box-sizing:border-box;\n" +
//                "background-color: #E6E6E6;\n" +
//                "border-bottom-left-radius:10px 10px;\n" +
//                "border-bottom-right-radius:10px 10px;\n" +
//                "border-top-left-radius:10px 10px;\n" +
//                "border-top-right-radius:10px 10px;\n" +
//                "font:normal normal normal 13px/normal Helvetica;\n" +
//                "overflow:auto;\n" +
//                "padding:10px;\n" +
//                "resize:none;\n" +
//                "width:500px;\n" +
//                "}\n" +
//                "</style>\n" +
//                "<script type=\"text/javascript\" src=\"//d1ivexoxmp59q7.cloudfront.net/godaddy/live.js\"></script><script type=\"text/javascript\" async=\"\" src=\"https://www.google-analytics.com/plugins/ga/inpage_linkid.js\" id=\"undefined\"></script><script type=\"text/javascript\" async=\"\" src=\"https://stats.g.doubleclick.net/dc.js\"></script><script src=\"//tags.tiqcdn.com/utag/godaddy/mobile/prod/utag.js\" type=\"text/javascript\" async=\"\"></script><script async=\"\" src=\"//www.googletagmanager.com/gtm.js?id=GTM-SXRF&amp;l=_gaDataLayer\"></script><script type=\"text/javascript\">\n" +
//                "var _gaDataLayer = _gaDataLayer || [];\n" +
//                "_gaDataLayer.push({ 'isc': '' });\n" +
//                "_gaDataLayer.push({ 'shopperId': '96062422' });\n" +
//                "_gaDataLayer.push({ 'server': 'P3PWCARTWEB110' });\n" +
//                "_gaDataLayer.push({ 'privateLabelId': '1' });\n" +
//                "var _gaq = _gaq || [];\n" +
//                "_gaq.push(['_setDomainName', 'godaddy.com']);\n" +
//                "_gaq.push(['_setCustomVar', 30, 'mblview', 'android', 3]);\n" +
//                "_gaq.push(['_setCustomVar', 32, 'mappid', '3', 3]);\n" +
//                "</script>\n" +
//                "<noscript>&lt;iframe src=\"//www.googletagmanager.com/ns.html?id=GTM-SXRF\" height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"&gt;&lt;/iframe&gt;</noscript>\n" +
//                "<script>\n" +
//                "(function (w, d, s, l, i) {\n" +
//                "w[l] = w[l] || [];\n" +
//                "w[l].push({ 'gtm.start': new Date().getTime(), event: 'gtm.js' });\n" +
//                "var f = d.getElementsByTagName(s)[0], j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';\n" +
//                "j.async = true;\n" +
//                "j.src = '//www.googletagmanager.com/gtm.js?id=' + i + dl;\n" +
//                "f.parentNode.insertBefore(j, f);\n" +
//                "})(window, document, 'script', '_gaDataLayer', 'GTM-SXRF');\n" +
//                "</script>\n" +
//                "<script type=\"text/javascript\" src=\"//tags.tiqcdn.com/utag/godaddy/mobile/prod/utag.40.js?utv=ut4.008.201412091905\" id=\"utag_godaddy.mobile_40\"></script><script type=\"text/javascript\" src=\"//tags.tiqcdn.com/utag/godaddy/mobile/prod/utag.47.js?utv=ut4.008.201412091905\" id=\"utag_godaddy.mobile_47\"></script><script type=\"text/javascript\" src=\"//tags.tiqcdn.com/utag/godaddy/mobile/prod/utag.75.js?utv=ut4.008.201412091905\" id=\"utag_godaddy.mobile_75\"></script><script type=\"text/javascript\" src=\"//www.googleadservices.com/pagead/conversion_async.js\"></script><script type=\"text/javascript\" src=\"//platform.twitter.com/oct.js\"></script></head>\n" +
//                "<body style=\"\"><iframe src='javascript:\"\"' id=\"__cvo_iframe\" style=\"width: 0px; height: 0px; border: 0px; position: absolute; left: -2000px;\"></iframe>\n" +
//                "<div id=\"MobileBodyContainer\">\n" +
//                "<div id=\"MobileContentContainer\">\n" +
//                "<div id=\"master-header\">\n" +
//                "<div id=\"divInfoMessage\" class=\"pad-med font-sm msg-info\" style=\"display:none;\"></div>\n" +
//                "<div id=\"divErrorMessage\" class=\"pad-med font-sm msg-attn\" style=\"display:none;\"></div>\n" +
//                "<div id=\"promoBanner\" class=\"promo-banner\" style=\"display: none;\"></div>\n" +
//                "</div>\n" +
//                "<div id=\"master-content\">\n" +
//                "<div id=\"basket-main-container\" class=\"main-bg\">\n" +
//                "<div class=\"row pad-med pad-tp-bt sep-bottom-bold\">\n" +
//                "<h5>Order Summary</h5>\n" +
//                "<h5 class=\"right\">\n" +
//                "$100.34\n" +
//                "</h5>\n" +
//                "</div>\n" +
//                "<form id=\"formModifyBasket\" name=\"formModifyBasket\" method=\"post\" action=\"https://cart.m.godaddy.com/ViewControls/Android/Cart/PostHandlers/BasketPostHandler.ashx?mDeviceID=e5820eb926e358c8dc48422a54b82462&amp;mappid=3&amp;hidetoolbar=1\">\n" +
//                "<input id=\"basket-update-privacy\" name=\"forumsubmitbutton\" type=\"submit\" value=\"updateprivacy\" style=\"display:none;\">\n" +
//                "<input id=\"basket-remove-item\" name=\"forumsubmitbutton\" type=\"submit\" value=\"removeitem\" style=\"display:none;\">\n" +
//                "<input id=\"basket-update-domain-length\" name=\"forumsubmitbutton\" type=\"submit\" value=\"updatelength\" style=\"display:none;\">\n" +
//                "<input id=\"cart-remove-item-id\" name=\"cart-remove-item-id\" type=\"hidden\" value=\"\">\n" +
//                "<div class=\"sep-bottom pad-med\">\n" +
//                "<div class=\"row\">\n" +
//                "<div class=\"max\">\n" +
//                "<div class=\"margin-bt font-sm bold wrap\">tycoon.menu </div>\n" +
//                "<div class=\"item-details\">\n" +
//                "<div class=\"font-xsm \"> .MENU Domain Name Registration - 2  Years</div>\n" +
//                "<div class=\"domain-term-length-container pad-rt margin-tp\">\n" +
//                "<select id=\"domain-length-1\" style=\"height:24px;width:auto\" onchange=\"changeDomainTermLength(this, 1);\">\n" +
//                "<option value=\"1\">1 Year</option>\n" +
//                "<option value=\"2\" selected=\"\">2 Years</option>\n" +
//                "<option value=\"3\">3 Years</option>\n" +
//                "<option value=\"5\">5 Years</option>\n" +
//                "<option value=\"10\">10 Years</option>\n" +
//                "</select>\n" +
//                "<input id=\"domain-term-length-hidden-1\" name=\"domain-term-length-hidden-1\" type=\"hidden\" value=\"2\">\n" +
//                "</div>\n" +
//                "<div id=\"child-Items\" class=\"margin-tp\">\n" +
//                "</div>\n" +
//                "</div>\n" +
//                "</div>\n" +
//                "<div class=\"pad-lt right\">\n" +
//                "<a id=\"product-remove-0\" class=\"font-sm\" onclick=\"removeItem(1)\">remove</a>\n" +
//                "<div class=\"nowrap margin-tp\">\n" +
//                "<div id=\"Item-price\" class=\"font-sm\">$99.98</div>\n" +
//                "</div>\n" +
//                "</div>\n" +
//                "</div>\n" +
//                "<div class=\"table margin-tp\">\n" +
//                "<input id=\"hdnPrivacy1\" name=\"hdnPrivacy1\" class=\"privacy-hidden\" type=\"hidden\" value=\"public\">\n" +
//                "<div>\n" +
//                "<div id=\"privacy-checkbox-1\" class=\"checkbox\" onclick=\"togglePrivacy(1)\">\n" +
//                "<span>✔</span>\n" +
//                "</div>\n" +
//                "</div>\n" +
//                "<div class=\"font-xsm pad-lt\"><span>Your personal info will be publicly available, make your domain private for </span><span class=\"bold\">$7.99</span></div>\n" +
//                "<div class=\"pad-lt center-rt\" style=\"min-width:50px\">\n" +
//                "<div class=\"nowrap font-sm\"></div>\n" +
//                "</div>\n" +
//                "</div>\n" +
//                "</div>\n" +
//                "</form>\n" +
//                "<form id=\"promo-form\" name=\"promo-form\" method=\"post\" action=\"https://cart.m.godaddy.com/ViewControls/Android/Cart/PostHandlers/BasketPostHandler.ashx?mDeviceID=e5820eb926e358c8dc48422a54b82462&amp;mappid=3&amp;hidetoolbar=1\">\n" +
//                "<div id=\"currentPromoCode\" class=\"row pad-med font-sm sep-bottom\">\n" +
//                "<label class=\"pad-lt-med\">Promo code</label>\n" +
//                "<div class=\"pad-rt center-rt\"><input id=\"promo-code-input\" name=\"promo-code-input\" type=\"text\" class=\"pad-lt-med\" style=\"height:24px;width:100px\" value=\"\"></div>\n" +
//                "<div class=\"center-rt\">\n" +
//                "<button id=\"promo-code-button\" class=\"btn-sm btn-gray\" type=\"button\">Save</button>\n" +
//                "</div>\n" +
//                "</div>\n" +
//                "<input id=\"itemCount\" value=\"1\" type=\"hidden\">\n" +
//                "<input id=\"submit-promo-code\" style=\"display:none\" name=\"forumsubmitbutton\" type=\"submit\" value=\"applypromocode\">\n" +
//                "</form>\n" +
//                "<div id=\"savings-price-row\" class=\"pad-tp-bt-med max sep-bottom clickable font-sm\" onclick=\"savingsDescription.showSlideMenu();\">\n" +
//                "<div class=\"row\">\n" +
//                "<div id=\"icann-fee-link\" class=\"cart-savings-link wrap\">ICANN Fees:</div>\n" +
//                "<div id=\"icann-fee-price\" class=\"cart-savings-price right pad-rt-med wrap\">$0.36</div>\n" +
//                "</div>\n" +
//                "</div>\n" +
//                "<form id=\"formBasket\" name=\"formBasket\" method=\"post\" action=\"https://cart.m.godaddy.com/viewcontrols/android/cart/posthandlers/checkoutposthandler.ashx?mDeviceID=e5820eb926e358c8dc48422a54b82462&amp;mappid=3&amp;hidetoolbar=1&amp;rand=43454\">\n" +
//                "<div id=\"total-price-row\" class=\"row pad-med pad-tp-bt sep-bottom font-med sep-top-bold\">\n" +
//                "<div class=\"bold\">Total due now</div>\n" +
//                "<div id=\"cart-total-price\" class=\"right nowrap bold\">$100.34</div>\n" +
//                "</div>\n" +
//                "<div class=\"pad-tp-bt pad-rt-med right\">\n" +
//                "<div id=\"offer-disclaimers\">\n" +
//                "<a class=\"font-xsm\" style=\"text-decoration:none\" href=\"https://cart.m.godaddy.com/cart/disclaimer.aspx?disclaimerType=iscdisclaimer&amp;mDeviceID=e5820eb926e358c8dc48422a54b82462&amp;mappid=3&amp;hidetoolbar=1&amp;ci=17515\">view offer disclaimers</a>\n" +
//                "</div>\n" +
//                "</div>\n" +
//                "<div class=\"pad-med\">\n" +
//                "<button id=\"continue-checkout\" type=\"button\" class=\"btn-med btn-buy max\" onclick=\"window.location = 'https://cart.m.godaddy.com/cart/placeorder.aspx?mDeviceID=e5820eb926e358c8dc48422a54b82462&amp;mappid=3&amp;hidetoolbar=1&amp;ci=17507';\">Proceed to Checkout</button>\n" +
//                "</div>\n" +
//                "</form>\n" +
//                "</div>\n" +
//                "<div id=\"divLoadingModal\" class=\"modal-cont\" style=\"display:none;-webkit-box-shadow: 0 0 30px 5px #333;\">\n" +
//                "<div id=\"loadingSpinner\">\n" +
//                "<img style=\"width:32px;height:32px;display: block;margin-left:auto;margin-right:auto;\" src=\"https://img1.wsimg.com/mobile/img/1/ajax-spinner.gif\" alt=\"Loading...\">\n" +
//                "</div>\n" +
//                "<div id=\"loadingSpinnerText\" class=\"font-med\"></div>\n" +
//                "</div>\n" +
//                "<div id=\"savings-description-slide\" class=\"main-bg max\" style=\"display:none;\">\n" +
//                "<div class=\"row pad-med pad-tp-bt sep-bottom font-sm\">\n" +
//                "<h4>Pricing Breakdown</h4>\n" +
//                "<div class=\"right\">\n" +
//                "<button id=\"savings-description-back-menu\" type=\"button\" class=\"btn-sm btn-gray\" onclick=\"savingsDescription.back();\">Back</button>\n" +
//                "</div>\n" +
//                "</div>\n" +
//                "<div id=\"icann-fee-description\" class=\"pad-med sep-bottom\">\n" +
//                "<div class=\"wrap font-sm\">\n" +
//                "<div class=\"bold\">ICANN Fee</div>\n" +
//                "<div>This mandatory fee is charged by ICANN (Internet Corporation for Assigned Names and Numbers: www.icann.org), and became effective on November 1, 2004.</div>\n" +
//                "<div>The ICANN fee for .com, .net, .org, .biz, .info, .asia, .jobs, .xxx, .mobi, .uno, .menu, .build and .luxury is $0.18 per domain name registration year. Thus, the fee on a one year .com registration would be $0.18. The fee would be $0.36 for a two year registration (i.e. $0.18 times 2). Additional years would be at $0.18.</div>\n" +
//                "</div>\n" +
//                "</div>\n" +
//                "</div>\n" +
//                "</div>\n" +
//                "<div id=\"master-footer\">\n" +
//                "<div id=\"IntFooterContainer\" style=\"font-size:90%; margin:20px 5px;\">\n" +
//                "<hr>\n" +
//                "<div>\n" +
//                "<form method=\"post\" action=\"https://cart.m.godaddy.com/viewcontrols/internal/posthandlers/internalfooterposthandler.ashx?refurl=https%3a%2f%2fcart.m.godaddy.com%2fcart%2fbasket.aspx%3fmDeviceID%3de5820eb926e358c8dc48422a54b82462%26mappid%3d3%26hidetoolbar%3d1%26ci%3d13930&amp;mDeviceID=e5820eb926e358c8dc48422a54b82462&amp;mappid=3&amp;hidetoolbar=1&amp;rand=43454\">\n" +
//                "<b>Change Currency:</b> <select name=\"ddlInternalCurrecnyType\" id=\"ddlInternalCurrecnyType\" class=\"\">\n" +
//                "<option value=\"AED\">AED (د.إ)</option>\n" +
//                "<option value=\"ARS\">ARS (AR$)</option>\n" +
//                "<option value=\"AUD\">AUD (A$)</option>\n" +
//                "<option value=\"BRL\">BRL (R$)</option>\n" +
//                "<option value=\"CAD\">CAD (C$)</option>\n" +
//                "<option value=\"CHF\">CHF (CHF)</option>\n" +
//                "<option value=\"CLP\">CLP (CL$)</option>\n" +
//                "<option value=\"CNY\">CNY (¥)</option>\n" +
//                "<option value=\"COP\">COP (CO$)</option>\n" +
//                "<option value=\"CZK\">CZK (Kč)</option>\n" +
//                "<option value=\"DKK\">DKK (kr)</option>\n" +
//                "<option value=\"EGP\">EGP (£)</option>\n" +
//                "<option value=\"EUR\">Euro (€)</option>\n" +
//                "<option value=\"GBP\">GBP (£)</option>\n" +
//                "<option value=\"HKD\">HKD (HK$)</option>\n" +
//                "<option value=\"HUF\">HUF (Ft)</option>\n" +
//                "<option value=\"IDR\">IDR (Rp)</option>\n" +
//                "<option value=\"ILS\">ILS (₪)</option>\n" +
//                "<option value=\"INR\">INR (₨)</option>\n" +
//                "<option value=\"JPY\">JPY (¥)</option>\n" +
//                "<option value=\"KRW\">KRW (₩)</option>\n" +
//                "<option value=\"MAD\">MAD (.د.م)</option>\n" +
//                "<option value=\"MXN\">MXN (MXN)</option>\n" +
//                "<option value=\"MYR\">MYR (RM)</option>\n" +
//                "<option value=\"NOK\">NOK (kr)</option>\n" +
//                "<option value=\"NZD\">NZD (NZ$)</option>\n" +
//                "<option value=\"PEN\">PEN (S/.)</option>\n" +
//                "<option value=\"PHP\">PHP (₱)</option>\n" +
//                "<option value=\"PKR\">PKR (₨)</option>\n" +
//                "<option value=\"PLN\">PLN (zł)</option>\n" +
//                "<option value=\"RON\">RON (lei)</option>\n" +
//                "<option value=\"RUB\">RUB (руб)</option>\n" +
//                "<option value=\"SAR\">SAR (﷼)</option>\n" +
//                "<option value=\"SEK\">SEK (kr)</option>\n" +
//                "<option value=\"SGD\">SGD (SG$)</option>\n" +
//                "<option value=\"THB\">THB (฿)</option>\n" +
//                "<option value=\"TRY\">TRY (TL)</option>\n" +
//                "<option value=\"TWD\">TWD (NT$)</option>\n" +
//                "<option value=\"UAH\">UAH (₴)</option>\n" +
//                "<option selected=\"selected\" value=\"USD\">USD ($)</option>\n" +
//                "<option value=\"UYU\">UYU ($U)</option>\n" +
//                "<option value=\"VEF\">VEF (Bs.)</option>\n" +
//                "<option value=\"VND\">VND (₫)</option>\n" +
//                "<option value=\"ZAR\">ZAR (R)</option>\n" +
//                "</select> <input name=\"forumsubmitbutton\" value=\"save\" type=\"submit\">\n" +
//                "</form>\n" +
//                "</div>\n" +
//                "<hr>\n" +
//                "<div><b>ProxyContext.LocalARR.HeaderStatus:</b> Empty</div>\n" +
//                "<hr>\n" +
//                "<div><b>ProxyContext.AkamaiDSA.HeaderStatus:</b> Empty</div>\n" +
//                "<hr>\n" +
//                "<div><b>ProxyContext.Status:</b> None</div>\n" +
//                "<hr>\n" +
//                "<div><b>ProxyContext.OriginIP:</b> 172.24.44.33</div>\n" +
//                "<hr>\n" +
//                "<div><b>ProxyContext.OriginHost:</b> cart..m..godaddy..com</div>\n" +
//                "<hr>\n" +
//                "<div><b>ProxyContext.ContextHost:</b> cart..m..godaddy..com</div>\n" +
//                "<hr>\n" +
//                "<div><b>ShopperContext.ShopperId.IdpCookies:</b> 96062422</div>\n" +
//                "<hr>\n" +
//                "<div><b>ShopperContext.Status.IdpCookies:</b> Public</div>\n" +
//                "<hr>\n" +
//                "<div><b>CurrencyPreference.CurrencyType:</b> USD</div>\n" +
//                "<hr>\n" +
//                "<div><b>CurrencyPreference.Source:</b> Preferences Cookie</div>\n" +
//                "<hr>\n" +
//                "<div><b>Has CDS Content:</b> False</div>\n" +
//                "<hr>\n" +
//                "<div><b>Slim Shopper:</b> False</div>\n" +
//                "<hr>\n" +
//                "<div><b>Shopper Price Type:</b> 0</div>\n" +
//                "<hr>\n" +
//                "<div><b>Private Label Id:</b> 1</div>\n" +
//                "<hr>\n" +
//                "<div><b>ISC:</b> </div>\n" +
//                "<hr>\n" +
//                "<div><b>Display Currency:</b> USD</div>\n" +
//                "<hr>\n" +
//                "<div><b>Transactional Currency:</b> USD</div>\n" +
//                "<hr>\n" +
//                "<div><b>Cart Transactional Currency:</b> USD</div>\n" +
//                "<hr>\n" +
//                "<div><b>In-Store Credit:</b> $0.00</div>\n" +
//                "<hr>\n" +
//                "<div><b>Server:</b> P3PWCARTWEB110</div>\n" +
//                "<hr>\n" +
//                "<div><b>CI:</b> 13930</div>\n" +
//                "<hr>\n" +
//                "<div><b>Pathway:</b> c19bae5e-edac-420b-b358-b95b61175b4d</div>\n" +
//                "<hr>\n" +
//                "<div><b>PageCount:</b> 0</div>\n" +
//                "<hr>\n" +
//                "<div><b>Split Value:</b> 78</div>\n" +
//                "<hr>\n" +
//                "<div><b>Cassandra Session:</b> Kol6+4HfBqc65OztqrR6hbdmRqvi7BxARAUp8RlKC8w=</div>\n" +
//                "<hr>\n" +
//                "<div><b>Traffic Guid:</b> c19bae5e-edac-420b-b358-b95b61175b4d</div>\n" +
//                "<hr>\n" +
//                "<div><b>Localization.CountrySite:</b> WWW</div>\n" +
//                "<hr>\n" +
//                "<div><b>Localization.FullLanguage:</b> en-US</div>\n" +
//                "<hr>\n" +
//                "<div><b>Localization.ShortLanguage:</b> en</div>\n" +
//                "<hr>\n" +
//                "<div><b>User Agent:</b> Mozilla/5.0 (Linux; Android 4.4.2; sdk Build/KK) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36</div>\n" +
//                "<hr>\n" +
//                "<div><b>IsBot:</b> False</div>\n" +
//                "<hr>\n" +
//                "<div><b>Mobile View:</b> android</div>\n" +
//                "<hr>\n" +
//                "<div><b>Browser:</b> Chrome</div>\n" +
//                "<hr>\n" +
//                "<div><b>Browser ID:</b> chrome</div>\n" +
//                "<hr>\n" +
//                "<div><b>Browser Version:</b> 30.0</div>\n" +
//                "<hr>\n" +
//                "<div><b>Browser User Languages:</b> en-US</div>\n" +
//                "<hr>\n" +
//                "<div><b>Screen Width:</b> <span id=\"spanWidth\">unknown</span></div>\n" +
//                "<hr>\n" +
//                "<div><b>Screen Height:</b> <span id=\"spanHeight\">unknown</span></div>\n" +
//                "<hr>\n" +
//                "<div><b>Build Tag:</b> mobile_cart-b0122194</div>\n" +
//                "<hr>\n" +
//                "<div><b>Cart XML:</b></div>\n" +
//                "<div>\n" +
//                "<textarea id=\"IntCartXml\" rows=\"25\" cols=\"20\" readonly=\"readonly\">&lt;ORDER&gt;&lt;ORDERDETAIL currencydisplay=\"USD\" _pipeline_error_level=\"1\" _pipeline_success=\"1\" overridepricingvar=\"0.0000\" prepaid_discount_total=\"0\" _old_spend=\"9998\" _old_set_up=\"1\" transactioncurrency=\"USD\" item_id_start=\"1\" createdate=\"1/2/2015 2:30:35 PM\" _oadjust_subtotal=\"10034\" ship_to_email=\"96062422\" mktg_partner=\"no\" _product_promo_list=\"42130\" _promo_run_time=\"16\" conversionratet2o=\"1.0000\" _total_total=\"10034\" mktg_mail=\"yes\" _tax_included=\"0\" mktg_nonpromotional_notices=\"yes\" addclientip=\"64.202.160.73\" shopper_id=\"96062422\" _old_discount=\"0\" mktg_email=\"yes\" operatingcurrency=\"USD\" payment=\"\" order_id=\"776497885\" basket_type=\"gdshop\" domaincount=\"1\" date_changed=\"1/2/2015 2:30:00 PM\" loginname=\"96062422\" conversionratet2d=\"1.0000\" _handling_total=\"0\" _shopper_shopper_id=\"96062422\" conversionratet2u=\"1.0000\" _shipping_total=\"0\" pricingmembershiplevel=\"0\" privatelabelid=\"1\" basket_paymenttype=\"-1\" bill_to_email=\"96062422\" conversionrateo2u=\"1.0000\" bar_restricted_payment_type=\"\" shipping_price=\"0\" itemcount=\"1\" _vertex_taxes=\"1\" _restricted_payment_type=\"\" merchantaccountid=\"1\" _base_subtotal=\"10034\" accountingcompanyid=\"1\" membershiplevel=\"0\" _purchase_pipeline_name=\"D:\\GDComponents\\Pipelines\\plan_pricing.xml\" _tax_total=\"0\" /&gt;&lt;ITEMS&gt;&lt;ITEM product_bulk_year=\"2\" _product_sale_start=\"1/1/2000\" coupon_group_id=\"\" dept_id=\"8\" _icann_fee=\"36\" _product_sale_price=\"9998\" cicode=\"80079\" list_price=\"9998\" _gdshop_product_domain_discountexists=\"0\" _product_sku=\"42130-1\" _glrevenueaccount=\"42026-0000-000\" pathway=\"c19bae5e-edac-420b-b358-b95b61175b4d\" bari_restricted_payment_type=\"\" _isdomaindiscounteligible=\"0\" _oadjust_discount=\"0\" _product_taxtypebit=\"0\" _gdshop_orderleveldiscounttypeid=\"3\" _description2=\".MENU Domain Registration\" product_isbundle=\"0\" _product_is_renewal_price_locked=\"1\" addclientip=\"64.202.160.73\" pf_id=\"42130\" _product_basketagelimit=\"2\" _base_pf_id=\"42128\" _actual_pricegroupid=\"0\" _iadjust_regularprice=\"9998\" recurring_payment=\"annual\" item_id=\"1\" trans_method=\"service\" _gdshop_product_type_fulfillmentmethodid=\"1\" _product_dept_id=\"8\" _transaction_currency_lockprice=\"9998\" zero=\"0\" min_shipping=\"650\" _product_prepaiddiscountlevel=\"0\" _distribute_eligible_list=\"9998\" status_id=\"1\" _actual_cost=\"5000\" _base_adjustedprice=\"10034\" period_description=\"Year(s)\" duration=\"1\" gdshop_product_typeid=\"2\" _old_list_price=\"9998\" domain=\"tycoon.menu\" _bsaccount=\"24051-0000-000\" _n_unadjusted=\"1\" _cosaccount=\"52026-0000-000\" tldtierid=\"19\" _is_base_on_sale=\"0\" product_isbulk=\"1\" _product_sale_end=\"1/1/2000\" unifiedproductid=\"42130\" date_added=\"1/2/2015 2:30:35 PM\" product_basketlocked=\"3\" _product_basketlocked=\"3\" name=\".MENU Domain Name Registration - 2  Years\" _product_unifiedproductid=\"42130\" _product_pf_id=\"42130\" _product_trans_method=\"service\" _product_price_locked=\"0\" placed_price=\"9998\" sku=\"42130-1\" numberofperiods=\"2\" _gdshop_variant_price_type_id=\"0\" _productpurchasetype=\"1\" quantitydescription=\"domain(s)\" _oadjust_adjustedprice=\"10034\" _product_list_price=\"9998\" product_hasfreedependentresource=\"0\" _iadjust_currentprice=\"9998\" overridepricerequired=\"0\" _product_name=\".MENU Domain Name Registration - 2  Years\" _icann_fee_adjusted_usd=\"36\" _icann_fee_usd=\"36\" isplcommissionable=\"1\" pricegroupid=\"0\" _icann_fee_adjusted=\"36\" itemtrackingcode=\"mob_android_dpp\" quantity=\"1\" _base_price=\"9998\" _row_id=\"0\"&gt;&lt;CUSTOMXML&gt;&lt;domainBulkRegistration&gt;&lt;domain sld=\"tycoon\" tld=\"menu\" isoingo=\"10\" period=\"2\" ns=\"ns51.domaincontrol.com ns52.domaincontrol.com\" autorenewflag=\"1\" agreements=\"|1_\" isProxy=\"0\"&gt;&lt;contactshopper inherit=\"1\" /&gt;&lt;/domain&gt;&lt;/domainBulkRegistration&gt;&lt;/CUSTOMXML&gt;&lt;/ITEM&gt;&lt;/ITEMS&gt;&lt;BASKETERRORS /&gt;&lt;PURCHASEERRORS /&gt;&lt;/ORDER&gt;</textarea>\n" +
//                "</div>\n" +
//                "<hr>\n" +
//                "<div id=\"footer-cart-xml\" style=\"display:none;\"><order><orderdetail currencydisplay=\"USD\" _pipeline_error_level=\"1\" _pipeline_success=\"1\" overridepricingvar=\"0.0000\" prepaid_discount_total=\"0\" _old_spend=\"9998\" _old_set_up=\"1\" transactioncurrency=\"USD\" item_id_start=\"1\" createdate=\"1/2/2015 2:30:35 PM\" _oadjust_subtotal=\"10034\" ship_to_email=\"96062422\" mktg_partner=\"no\" _product_promo_list=\"42130\" _promo_run_time=\"16\" conversionratet2o=\"1.0000\" _total_total=\"10034\" mktg_mail=\"yes\" _tax_included=\"0\" mktg_nonpromotional_notices=\"yes\" addclientip=\"64.202.160.73\" shopper_id=\"96062422\" _old_discount=\"0\" mktg_email=\"yes\" operatingcurrency=\"USD\" payment=\"\" order_id=\"776497885\" basket_type=\"gdshop\" domaincount=\"1\" date_changed=\"1/2/2015 2:30:00 PM\" loginname=\"96062422\" conversionratet2d=\"1.0000\" _handling_total=\"0\" _shopper_shopper_id=\"96062422\" conversionratet2u=\"1.0000\" _shipping_total=\"0\" pricingmembershiplevel=\"0\" privatelabelid=\"1\" basket_paymenttype=\"-1\" bill_to_email=\"96062422\" conversionrateo2u=\"1.0000\" bar_restricted_payment_type=\"\" shipping_price=\"0\" itemcount=\"1\" _vertex_taxes=\"1\" _restricted_payment_type=\"\" merchantaccountid=\"1\" _base_subtotal=\"10034\" accountingcompanyid=\"1\" membershiplevel=\"0\" _purchase_pipeline_name=\"D:\\GDComponents\\Pipelines\\plan_pricing.xml\" _tax_total=\"0\"><items><item product_bulk_year=\"2\" _product_sale_start=\"1/1/2000\" coupon_group_id=\"\" dept_id=\"8\" _icann_fee=\"36\" _product_sale_price=\"9998\" cicode=\"80079\" list_price=\"9998\" _gdshop_product_domain_discountexists=\"0\" _product_sku=\"42130-1\" _glrevenueaccount=\"42026-0000-000\" pathway=\"c19bae5e-edac-420b-b358-b95b61175b4d\" bari_restricted_payment_type=\"\" _isdomaindiscounteligible=\"0\" _oadjust_discount=\"0\" _product_taxtypebit=\"0\" _gdshop_orderleveldiscounttypeid=\"3\" _description2=\".MENU Domain Registration\" product_isbundle=\"0\" _product_is_renewal_price_locked=\"1\" addclientip=\"64.202.160.73\" pf_id=\"42130\" _product_basketagelimit=\"2\" _base_pf_id=\"42128\" _actual_pricegroupid=\"0\" _iadjust_regularprice=\"9998\" recurring_payment=\"annual\" item_id=\"1\" trans_method=\"service\" _gdshop_product_type_fulfillmentmethodid=\"1\" _product_dept_id=\"8\" _transaction_currency_lockprice=\"9998\" zero=\"0\" min_shipping=\"650\" _product_prepaiddiscountlevel=\"0\" _distribute_eligible_list=\"9998\" status_id=\"1\" _actual_cost=\"5000\" _base_adjustedprice=\"10034\" period_description=\"Year(s)\" duration=\"1\" gdshop_product_typeid=\"2\" _old_list_price=\"9998\" domain=\"tycoon.menu\" _bsaccount=\"24051-0000-000\" _n_unadjusted=\"1\" _cosaccount=\"52026-0000-000\" tldtierid=\"19\" _is_base_on_sale=\"0\" product_isbulk=\"1\" _product_sale_end=\"1/1/2000\" unifiedproductid=\"42130\" date_added=\"1/2/2015 2:30:35 PM\" product_basketlocked=\"3\" _product_basketlocked=\"3\" name=\".MENU Domain Name Registration - 2  Years\" _product_unifiedproductid=\"42130\" _product_pf_id=\"42130\" _product_trans_method=\"service\" _product_price_locked=\"0\" placed_price=\"9998\" sku=\"42130-1\" numberofperiods=\"2\" _gdshop_variant_price_type_id=\"0\" _productpurchasetype=\"1\" quantitydescription=\"domain(s)\" _oadjust_adjustedprice=\"10034\" _product_list_price=\"9998\" product_hasfreedependentresource=\"0\" _iadjust_currentprice=\"9998\" overridepricerequired=\"0\" _product_name=\".MENU Domain Name Registration - 2  Years\" _icann_fee_adjusted_usd=\"36\" _icann_fee_usd=\"36\" isplcommissionable=\"1\" pricegroupid=\"0\" _icann_fee_adjusted=\"36\" itemtrackingcode=\"mob_android_dpp\" quantity=\"1\" _base_price=\"9998\" _row_id=\"0\"><customxml><domainbulkregistration><domain sld=\"tycoon\" tld=\"menu\" isoingo=\"10\" period=\"2\" ns=\"ns51.domaincontrol.com ns52.domaincontrol.com\" autorenewflag=\"1\" agreements=\"|1_\" isproxy=\"0\"><contactshopper inherit=\"1\"></contactshopper></domain></domainbulkregistration></customxml></item></items><basketerrors><purchaseerrors></purchaseerrors></basketerrors></orderdetail></order></div>\n" +
//                "</div>\n" +
//                "</div>\n" +
//                "</div>\n" +
//                "<div id=\"menu-overlay\"></div>\n" +
//                "<div>\n" +
//                "<div id=\"divTrafficImageContainer\" style=\"position:relative;height:0;\">\n" +
//                "<img style=\"position:absolute;top:0;left:0;\" src=\"https://img.godaddy.com/image.aspx?ci=13930&amp;page=%2fcart%2fbasket.aspx&amp;referrer=http%3a%2f%2fm.godaddy.com%2fdomains%2fdomainsearchlist.aspx%3fmDeviceID%3de5820eb926e358c8dc48422a54b82462%26mappid%3d3%26hidetoolbar%3d1%26ci%3d13945&amp;sitename=cart.m.godaddy.com&amp;server=P3PWCARTWEB110&amp;privatelabelid=1&amp;shopper=96062422&amp;querystring=mDeviceID%253de5820eb926e358c8dc48422a54b82462%2526mappid%253d3%2526hidetoolbar%253d1%2526ci%253d13930%2526mblview%253dandroid%2526&amp;rand=130413\" alt=\"\">\n" +
//                "</div>\n" +
//                "</div>\n" +
//                "</div>\n" +
//                "<script src=\"//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js\"></script>\n" +
//                "<script type=\"text/javascript\" src=\"https://img1.wsimg.com/mobile/js/mobile-core.min.20141123.js\"></script>\n" +
//                "<script type=\"text/javascript\">\n" +
//                "var fastballTrackingProvider = new mobile.fastball.fastballTrackingProvider();\n" +
//                "fastballTrackingProvider.init(\"https://img.godaddy.com/\", \"godaddy.com\");\n" +
//                "</script>\n" +
//                "<script type=\"text/javascript\">\n" +
//                "function SetInfoMessageText(html) {\n" +
//                "if (html != null && html != undefined && html.indexOf(\"<p>\") == -1)\n" +
//                "{\n" +
//                "html = \"<p>\" + html + \"</p>\";\n" +
//                "}\n" +
//                "// Used .focus to force the styles to take effect. For some reason, the padding\n" +
//                "// does not take effect on multiple info lines with -webkit-box display, until you hover the error box.\n" +
//                "$(\"#divInfoMessage\").focus().html(html).css(\"display\", \"-webkit-box\");\n" +
//                "$(window).scrollTop(0, 1);\n" +
//                "}\n" +
//                "function ClearInfoMessageText() {\n" +
//                "$(\"#divInfoMessage\").html(\"\").hide();\n" +
//                "}\n" +
//                "</script>\n" +
//                "<script type=\"text/javascript\">\n" +
//                "function SetErrorMessageText(html) {\n" +
//                "if (html != null && html != undefined && html.indexOf(\"<p>\") == -1)\n" +
//                "{\n" +
//                "html = \"<p>\" + html + \"</p>\";\n" +
//                "}\n" +
//                "// Used .focus to force the styles to take effect. For some reason, the padding\n" +
//                "// does not take effect on multiple error lines with -webkit-box display, until you hover the error box.\n" +
//                "$(\"#divErrorMessage\").focus().html(html).css(\"display\", \"\");\n" +
//                "$(window).scrollTop(0, 1);\n" +
//                "}\n" +
//                "function ClearErrorMessageText() {\n" +
//                "$(\"#divErrorMessage\").html(\"\").hide();\n" +
//                "}\n" +
//                "function tryGetInputValue(fieldName) {\n" +
//                "var inputVal = '';\n" +
//                "try {\n" +
//                "if (fieldName != null && fieldName != undefined && fieldName != '') {\n" +
//                "var inputValEl = $('input').filter(function() {\n" +
//                "if ($(this).attr('name') != undefined) { return $(this).attr('name').toLowerCase().indexOf(fieldName) > -1; } else { return null; }\n" +
//                "});\n" +
//                "if (inputValEl != null && inputValEl != undefined && inputValEl.length > 0) { inputVal = inputValEl.val(); }\n" +
//                "}\n" +
//                "} catch (ex) {}\n" +
//                "return inputVal;\n" +
//                "}\n" +
//                "function sendFormErrorGaFromHtml(form, fieldname, inputVal, html) {\n" +
//                "if (html.indexOf(\"<p>\") == -1) {\n" +
//                "html = \"<p>\" + html + \"</p>\";\n" +
//                "}\n" +
//                "var errorsHtml = $.parseHTML(html);\n" +
//                "$(errorsHtml).each(function (i, e) {\n" +
//                "sendFormErrorGa(form, fieldname, inputVal, $(e).text());\n" +
//                "});\n" +
//                "}\n" +
//                "var _errorSent = false;\n" +
//                "function sendFormErrorGa(form, fieldname, inputVal, vError) {\n" +
//                "try {\n" +
//                "if (!_errorSent) {\n" +
//                "if (form == null || form == undefined || form == '') { form = 'N/A'; }\n" +
//                "if (fieldname == null || fieldname == undefined || fieldname == '') { fieldname = 'N/A'; }\n" +
//                "if (inputVal == null || inputVal == undefined || inputVal == '') { inputVal = 'empty'; }\n" +
//                "if (vError == null || vError == undefined || vError == '') { vError = 'N/A'; }\n" +
//                "sendFormGa('Form_' + form, '' + fieldname + \"|\" + vError, '' + inputVal);\n" +
//                "_errorSent = true;\n" +
//                "}\n" +
//                "} catch (ex) { }\n" +
//                "}\n" +
//                "function sendFormSubmitGa(form, count, _callback) {\n" +
//                "var callbackHit = false;\n" +
//                "_callback = _callback || function () { };\n" +
//                "var callback = function () {\n" +
//                "if (!callbackHit) {\n" +
//                "callbackHit = true;\n" +
//                "_callback();\n" +
//                "}\n" +
//                "};\n" +
//                "try {\n" +
//                "if (form == null || form == undefined || form == '') { form = 'N/A'; }\n" +
//                "if (count == null || count == undefined || count == '') { count = \"1\"; }\n" +
//                "if (_gaq != undefined) {\n" +
//                "_gaq.push(['_set', 'hitCallback', function () {\n" +
//                "callback();\n" +
//                "}]);\n" +
//                "_gaq.push(['_trackEvent', 'Form_' + form, 'Submit', '' + count, 1]);\n" +
//                "} else {\n" +
//                "callback();\n" +
//                "}\n" +
//                "_errorSent = false;\n" +
//                "} catch (ex) {\n" +
//                "callback();\n" +
//                "}\n" +
//                "setTimeout(function () {\n" +
//                "callback();\n" +
//                "}, 3000);\n" +
//                "}\n" +
//                "function sendFormGa(cat, action, label) {\n" +
//                "var gaq = _gaq || [];\n" +
//                "setTimeout(function () { gaq.push(['_trackEvent', cat, action, label, 1]); }, 100);\n" +
//                "}\n" +
//                "</script>\n" +
//                "<script type=\"text/javascript\" language=\"javascript\">\n" +
//                "$(document).ready(\n" +
//                "function() {\n" +
//                "//setup ajax call to get and load banners\n" +
//                "$.ajax({\n" +
//                "type: \"GET\",\n" +
//                "contentType: \"application/json; charset=utf-8\",\n" +
//                "url: \"https://cart.m.godaddy.com/viewcontrols/android/shared/ajaxhandlers/promobannerajaxhandler.ashx?mDeviceID=e5820eb926e358c8dc48422a54b82462&mappid=3&hidetoolbar=1&rand=460293&ajaxmethod=getpromobanners\",\n" +
//                "data: '',\n" +
//                "dataType: \"json\",\n" +
//                "success: function (msg)\n" +
//                "{\n" +
//                "var promoBanner = $(\"#promoBanner\");\n" +
//                "//foreach item returned, write out banner\n" +
//                "for (var iCounter = 0; iCounter < msg.length; iCounter++)\n" +
//                "{\n" +
//                "html = \"<div><span>\" + msg[iCounter].BannerText + \"</span></div>\";\n" +
//                "if (msg[iCounter].DetailsUrl != null) {\n" +
//                "html += \"<div><a class='font-sm color-white' href=\" + msg[iCounter].DetailsUrl + \">Full Details</a></div>\";\n" +
//                "}\n" +
//                "promoBanner.html(html);\n" +
//                "promoBanner.slideDown(500);\n" +
//                "}\n" +
//                "}\n" +
//                "});\n" +
//                "});\n" +
//                "</script>\n" +
//                "<script type=\"text/javascript\" language=\"javascript\">\n" +
//                "function togglePrivacy(itemId) {\n" +
//                "var hidden = $(\"#hdnPrivacy\" + itemId);\n" +
//                "if (hidden.val() === 'private') {\n" +
//                "$(\"#privacy-checkbox-\" + itemId).removeClass(\"checked\");\n" +
//                "hidden.val(\"public\");\n" +
//                "}\n" +
//                "else {\n" +
//                "$(\"#privacy-checkbox-\" + itemId).addClass(\"checked\");\n" +
//                "hidden.val(\"private\");\n" +
//                "}\n" +
//                "loadingModal.show(\"Updating...\");\n" +
//                "$(\"#basket-update-privacy\").click();\n" +
//                "}\n" +
//                "function removeItem(itemId) {\n" +
//                "$(\"#cart-remove-item-id\").val(itemId);\n" +
//                "loadingModal.show(\"Updating...\");\n" +
//                "$(\"#basket-remove-item\").click();\n" +
//                "}\n" +
//                "function changeDomainTermLength(sel, itemId) {\n" +
//                "$(\"#domain-term-length-hidden-\" + itemId).val(sel.value);\n" +
//                "loadingModal.show(\"Updating...\");\n" +
//                "$(\"#basket-update-domain-length\").click();\n" +
//                "}\n" +
//                "</script>\n" +
//                "<script type=\"text/javascript\">\n" +
//                "$(document).ready(function() {\n" +
//                "loadingModal.initialize();\n" +
//                "});\n" +
//                "var loadingModal = {\n" +
//                "modal: null,\n" +
//                "initialize: function() {\n" +
//                "this.modal = $(\"#divLoadingModal\");\n" +
//                "},\n" +
//                "show: function(msg, options) {\n" +
//                "if(!msg) { msg = \"Loading...\"; }\n" +
//                "$(this.modal.children()[1]).html(msg);\n" +
//                "this.modal.data(\"rotatecount\", 0);\n" +
//                "if (options && options.keepalive === true) {\n" +
//                "this.modal.data(\"timecount\", 0);\n" +
//                "}\n" +
//                "else {\n" +
//                "this.modal.data(\"timeouthandle\", 100);\n" +
//                "this.modal.data(\"timecount\", 0);\n" +
//                "}\n" +
//                "this.modal.showModal()\n" +
//                "},\n" +
//                "hide: function(callback) {\n" +
//                "clearTimeout(this.modal.data(\"timeouthandle\"));\n" +
//                "clearTimeout(this.modal.data(\"delayid\"));\n" +
//                "var callbackTimeout = 500;\n" +
//                "if(this.modal.data(\"timecount\") < 1000) {\n" +
//                "setTimeout(function () { loadingModal.modal.closeModal(); }, 1000);\n" +
//                "callbackTimeout = 1500;\n" +
//                "}\n" +
//                "else {\n" +
//                "this.modal.closeModal();\n" +
//                "}\n" +
//                "if(callback) {\n" +
//                "setTimeout(callback, callbackTimeout);\n" +
//                "}\n" +
//                "}\n" +
//                "};\n" +
//                "</script>\n" +
//                "<script type=\"text/javascript\" language=\"javascript\">\n" +
//                "var cartSlide = function (initMenuId) {\n" +
//                "this.mainMenuId = initMenuId;\n" +
//                "this.menuItems = [this.mainMenuId];\n" +
//                "};\n" +
//                "cartSlide.prototype.showSlideMenu = function(id, callbackFunction) {\n" +
//                "var currentMenu = $(\"#\" + this.mainMenuId);\n" +
//                "if (this.menuItems.length > 0)\n" +
//                "currentMenu = $(\"#\" + this.menuItems[this.menuItems.length - 1]);\n" +
//                "this.menuItems.push(id);\n" +
//                "currentMenu.openSlideMenu({ slideMenu: id, hide: [\"btnBack\", \"ApplicationFooterContainer\", \"IntFooterContainer\"], callback: callbackFunction });\n" +
//                "};\n" +
//                "cartSlide.prototype.previousSlideMenu = function(callbackFunction) {\n" +
//                "var currentSlideMenuId = this.mainMenuId;\n" +
//                "if (this.menuItems.length > 0)\n" +
//                "currentSlideMenuId = this.menuItems[this.menuItems.length - 1];\n" +
//                "this.menuItems.pop();\n" +
//                "var prevMenu = $('#' + this.menuItems[this.menuItems.length - 1]);\n" +
//                "if (this.menuItems.length <= 1) {\n" +
//                "prevMenu.closeSlideMenu({ slideMenu: currentSlideMenuId, show: [\"btnBack\", \"ApplicationFooterContainer\", \"IntFooterContainer\"], callback: callbackFunction });\n" +
//                "} else {\n" +
//                "prevMenu.closeSlideMenu({ slideMenu: currentSlideMenuId, hide: [\"btnBack\", \"ApplicationFooterContainer\", \"IntFooterContainer\"], callback: callbackFunction });\n" +
//                "}\n" +
//                "};\n" +
//                "cartSlide.prototype.initMoveHeader = function(mainContainerId) {\n" +
//                "var search = $(\"#search-sel-cont\");\n" +
//                "if (search.length > 0)\n" +
//                "search.insertBefore(\"#\" + mainContainerId + \" > :first-child\");\n" +
//                "var hdr = $(\"#main-hdr\");\n" +
//                "if (hdr.length > 0)\n" +
//                "hdr.insertBefore(\"#\" + mainContainerId + \" > :first-child\");\n" +
//                "};\n" +
//                "</script>\n" +
//                "<script type=\"text/javascript\" language=\"javascript\">\n" +
//                "$(document).bind(\"ready\", function () {\n" +
//                "cart.initialize();\n" +
//                "savingsDescription.initialize();\n" +
//                "$(\"#find-more-domains\").click(function () {\n" +
//                "if (typeof (_gaq) != \"undefined\") {\n" +
//                "_gaq.push(['_trackEvent', 'Cart', 'Click To Add more domains to your order', 'Click Add more domains to your order', 1]);\n" +
//                "}\n" +
//                "});\n" +
//                "});\n" +
//                "var cart = {\n" +
//                "doDomainsAjaxValidation: true,\n" +
//                "isFromLoginRedirect: false,\n" +
//                "initialize: function () {\n" +
//                "var promoCodeError = \"\";\n" +
//                "if (promoCodeError.length > 0) {\n" +
//                "window.alert(promoCodeError);\n" +
//                "}\n" +
//                "$(\"#promo-code-button\").click(function () {\n" +
//                "cart.submitPromo();\n" +
//                "});\n" +
//                "$(\"#promo-code-input\").keypress(function (e) {\n" +
//                "if (e.which == 13) {\n" +
//                "e.preventDefault();\n" +
//                "cart.submitPromo();\n" +
//                "}\n" +
//                "});\n" +
//                "},\n" +
//                "submitPromo: function() {\n" +
//                "if ($(\"#promo-code-input\").val().trim().length == 0) {\n" +
//                "SetErrorMessageText(\"Enter promo, source or referral\\u00AC code\");\n" +
//                "}\n" +
//                "else {\n" +
//                "loadingModal.show(\"Updating...\");\n" +
//                "$(\"#submit-promo-code\").click();\n" +
//                "}\n" +
//                "},\n" +
//                "show: function () {\n" +
//                "$(\"#basket-main-container\").show();\n" +
//                "},\n" +
//                "hide: function () {\n" +
//                "$(\"#basket-main-container\").hide();\n" +
//                "},\n" +
//                "clickLinkPromoCode: function () {\n" +
//                "$(\"#divPromoCode\").show();\n" +
//                "$(\"#currentPromoCode\").hide();\n" +
//                "$(\"#enter-promo-code\").hide();\n" +
//                "},\n" +
//                "submitBasket: function(submitId) {\n" +
//                "$(\"#\" + submitId).trigger('click');\n" +
//                "},\n" +
//                "paypalClick: function() {\n" +
//                "cart.doDomainsAjaxValidation ? this.validateDomainContacts(\"paypalexpress-submit\") :  $(\"#paypalexpress-submit\").click();;\n" +
//                "},\n" +
//                "validateDomainContacts: function (submitId) {\n" +
//                "var post = formProvider.getFormData($(\"#formBasket\"));\n" +
//                "loadingModal.show(\"Please wait...\");\n" +
//                "$.ajax({\n" +
//                "type: \"GET\",\n" +
//                "url: \"https://cart.m.godaddy.com/viewcontrols/android/cart/ajaxhandlers/basketdomaincontactsajaxhandler.ashx?mDeviceID=e5820eb926e358c8dc48422a54b82462&mappid=3&hidetoolbar=1&rand=43454&ajaxmethod=contactupdatepd\",\n" +
//                "data: post,\n" +
//                "dataType: \"json\",\n" +
//                "success: function(data) {\n" +
//                "ClearErrorMessageText();\n" +
//                "ClearInfoMessageText();\n" +
//                "if (data != null) {\n" +
//                "if (data.success === true) {\n" +
//                "cart.submitBasket(submitId);\n" +
//                "} else {\n" +
//                "window.location = data.url;\n" +
//                "}\n" +
//                "}\n" +
//                "},\n" +
//                "error: function(req, status, err) {\n" +
//                "console.log(\"req: \" + req);\n" +
//                "loadingModal.hide();\n" +
//                "SetErrorMessageText(\"<p>Please try again or contact customer service..</p>\");\n" +
//                "}\n" +
//                "});\n" +
//                "}\n" +
//                "};\n" +
//                "var savingsDescription = {\n" +
//                "slide: null,\n" +
//                "initialize: function() {\n" +
//                "this.slide = new cartSlide(\"basket-main-container\");\n" +
//                "},\n" +
//                "showSlideMenu: function() {\n" +
//                "this.slide.showSlideMenu(\"savings-description-slide\");\n" +
//                "},\n" +
//                "back: function() {\n" +
//                "this.slide.previousSlideMenu();\n" +
//                "}\n" +
//                "};\n" +
//                "function showPricingDetails(phase) {\n" +
//                "$(\"#pricing-details-\" + phase).show();\n" +
//                "}\n" +
//                "function hidePricingDetails(detailsDiv) {\n" +
//                "$(detailsDiv).hide();\n" +
//                "}\n" +
//                "</script>\n" +
//                "<script type=\"text/javascript\" language=\"javascript\">\n" +
//                "var SiteHistory = {};\n" +
//                "$(document).bind(\"ready\", function() {\n" +
//                "$(document).on(\"click\", \".open-menu, .close-menu\", function (e) {\n" +
//                "var $this = $(this);\n" +
//                "var event = $this.data(\"event\");\n" +
//                "if (event == null || event == undefined)\n" +
//                "{\n" +
//                "if ($this.hasClass(\"close-menu\"))\n" +
//                "{\n" +
//                "data = {name: \"home\", id: \"\", href: data.href};\n" +
//                "}\n" +
//                "else\n" +
//                "{\n" +
//                "data = {\n" +
//                "name: \"menu\",\n" +
//                "id: $this.attr(\"id\"),\n" +
//                "href: $this.data(\"href\") || window.location,\n" +
//                "event: $this.hasClass(\"open-menu\") ? \"forward\" : \"back\"\n" +
//                "}\n" +
//                "}\n" +
//                "history.pushState(JSON.stringify(data), \"\", data.href);\n" +
//                "}\n" +
//                "});\n" +
//                "});\n" +
//                "SiteHistory.handleEventState = function(state) {\n" +
//                "if (state) {\n" +
//                "try {\n" +
//                "var data = JSON.parse(state);\n" +
//                "switch (data.name) {\n" +
//                "case \"menu\":\n" +
//                "$(\"#\" + data.id).data(\"event\", \"menuevent\");\n" +
//                "$(\"#\" + data.id).click();\n" +
//                "$(\"#\" + data.id).data(\"event\", null);\n" +
//                "break;\n" +
//                "case \"home\":\n" +
//                "SiteHistory.restorePage();\n" +
//                "break;\n" +
//                "}\n" +
//                "} catch(err) {\n" +
//                "}\n" +
//                "} else {\n" +
//                "history.replaceState(JSON.stringify({ name: \"home\", id: \"\", href: window.location.href }), \"\", window.location.href);\n" +
//                "}\n" +
//                "};\n" +
//                "SiteHistory.restorePage = function() {\n" +
//                "window.location.href = window.location.href;\n" +
//                "};\n" +
//                "window.addEventListener('popstate', function(e) {\n" +
//                "SiteHistory.handleEventState(e.state);\n" +
//                "});\n" +
//                "history.replaceState(JSON.stringify({name: \"home\", id: \"\", href: window.location.href}), \"\", window.location.href);\n" +
//                "</script>\n" +
//                "<script type=\"text/javascript\">\n" +
//                "(function (a, b, c, d) {\n" +
//                "a = '//tags.tiqcdn.com/utag/godaddy/mobile/prod/utag.js';\n" +
//                "b = document; c = 'script'; d = b.createElement(c); d.src = a; d.type = 'text/java' + c; d.async = true;\n" +
//                "a = b.getElementsByTagName(c)[0]; a.parentNode.insertBefore(d, a);\n" +
//                "})();\n" +
//                "</script>\n" +
//                "<script type=\"text/javascript\">\n" +
//                "var utag_data = {\n" +
//                "app_name:\"mobile.cart\",\n" +
//                "page_name:\"basket\",\n" +
//                "context_id:\"1\",\n" +
//                "pl_id:\"1\",\n" +
//                "source_code:\"\"\n" +
//                "}\n" +
//                "</script>\n" +
//                "\n" +
//                "<script type=\"text/javascript\" id=\"\">var _gaq=_gaq||[];\n" +
//                "(function(){_gaq.push([\"_require\",\"inpage_linkid\",\"//www.google-analytics.com/plugins/ga/inpage_linkid.js\"]);_gaq.push([\"_setAccount\",\"UA-37178807-1\"]);_gaq.push([\"_setAllowLinker\",!0]);_gaq.push([\"_setSessionCookieTimeout\",12E5]);var a=google_tag_manager[\"GTM-SXRF\"].macro('gtm1420234236670');\"string\"===typeof a&&0<a.length?_gaq.push([\"_link\",a]):(a=\"\",_gaq.push([\"_setCustomVar\",1,\"shopperId\",google_tag_manager[\"GTM-SXRF\"].macro('gtm1420234236671')||a,3]),_gaq.push([\"_setCustomVar\",2,\"privateLabelId\",google_tag_manager[\"GTM-SXRF\"].macro('gtm1420234236672')||a,3]),_gaq.push([\"_setCustomVar\",3,\"isc\",google_tag_manager[\"GTM-SXRF\"].macro('gtm1420234236673')||\n" +
//                "a,3]),_gaq.push([\"_setCustomVar\",4,\"server\",google_tag_manager[\"GTM-SXRF\"].macro('gtm1420234236674')||a,3]),_gaq.push([\"_setCustomVar\",5,\"segmentId\",google_tag_manager[\"GTM-SXRF\"].macro('gtm1420234236675')||a,3]),_gaq.push([\"_trackPageview\"]));a=typeof _gaPageLoad;\"function\"===a&&_gaPageLoad(_gaq);a=document.createElement(\"script\");a.type=\"text/javascript\";a.async=!0;a.src=(\"https:\"==document.location.protocol?\"https://\":\"http://\")+\"stats.g.doubleclick.net/dc.js\";var b=document.getElementsByTagName(\"script\")[0];b.parentNode.insertBefore(a,b)})();</script></body></html>";
//
//        webview.loadData(html, "text/html", null);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
